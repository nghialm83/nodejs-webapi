const router = require('express').Router();
var db = require('../db');

//Get all user
router.get('/', function(req, res){
    var sql = require('mssql');

    //Initiallising connection string
    var dbConfig = {
        user:  'sa',
        password: '123456',
        server: 'localhost',
        database: 'Nodejs'
    };

    //connect sql
    sql.connect(dbConfig, function (err){
        if (err) console.log(err);
        //create request object
        var request = new sql.Request();

        //query to database and get the records
        request.query('select * from [user]', function(err, recordset){
            if (err) console.log(err);

            //send record as a response
            res.send(recordset.recordset);
        });
    });
});


module.exports = router;