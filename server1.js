var express = require('express');
var app = express();
var user_router = require('./routes/routes.User');
var hero_router = require('./routes/routes.Hero');

app.use('/', user_router);
app.use('/', hero_router);

var server = app.listen(3000, function () {
    console.log('Server is running..');
});